#!/bin/bash

# Define variables  
cwd=$(pwd)
file_name=miniconda3-latest-Linux-x86_64.sh
mini_conda_url=https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

# Define directories
download_directory=$cwd/downloads
miniconda_directory=$cwd/miniconda
ci_directory=$cwd/ci

# Create directories
mkdir -p $download_directory
mkdir -p $miniconda_directory
  
# Download miniconda from website
wget -O $download_directory/$file_name $mini_conda_url

# Remove existing miniconda
rm -rf $miniconda_directory

# Run install miniconda script
bash $download_directory/$file_name -b -u -p $miniconda_directory

# Remove downloaded file
rm -rf  $download_directory

# Create conda environment 
$miniconda_directory/bin/conda create -n pyta --quiet --yes --file $ci_directory/pyta_environment_linux.yml

# Activate conda environment
source $miniconda_directory/bin/activate pyta

# Install pyta
python $cwd/setup.py install
