#!/bin/bash

# Define variables  
cwd=$(pwd)

# Define directories
miniconda_directory=$cwd/miniconda

# Activate conda environment
source $miniconda_directory/bin/activate pyta

# Install pyta
pytest --cov-report html:$cwd/tests/cov_html
