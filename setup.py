

from setuptools import setup, find_packages

setup(
	name 		= "pyta",
	author 		= "Anthony Rey",
	author_email	= "anthonyrey.simonnot@gmail.com",
	url 		= "https://gitlab.com/Vagrid/pyta",
	packages 	= find_packages(exclude = ["docs", "tests"]),
)
