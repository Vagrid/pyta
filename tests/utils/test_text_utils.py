
import pytest

from pyta.pyta.utils.text_utils import find_word_occurrances

class TestTextUtils:
	@pytest.mark.parametrize("test_input, expected",
				 [
				  (["is", "or", "is", "tell"], {"is":2, "or":1, "tell":1}),
				  (["Je", "veux", "du", "pain"], {"Je":1, "veux":1, "du":1, "pain":1}),
				 ])
	def test_find_word_occurrances(self, test_input, expected):	
		find_word_occurrances(test_input) == expected


